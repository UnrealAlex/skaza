// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Public/Structs.h"
#include "Components/SphereComponent.h"
#include "Components/SceneComponent.h"
#include "Cell.generated.h"

UCLASS()
class SKAZA_API ACell : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACell();

	UPROPERTY(BlueprintReadOnly)
		FJsonStruct CellInfo;

	UPROPERTY()
		USphereComponent* SphereCollision;

	UPROPERTY(EditDefaultsOnly)
		USceneComponent* Root;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
