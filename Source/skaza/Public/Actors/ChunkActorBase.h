// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "ProceduralMeshComponent.h"
#include "Public/Structs.h"
#include "Cell.h"
#include "Components/SphereComponent.h"
#include "ChunkActorBase.generated.h"




UCLASS()
class SKAZA_API AChunkActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AChunkActorBase();

	UPROPERTY(EditDefaultsOnly)
		UProceduralMeshComponent* Mesh1;

	UPROPERTY(EditDefaultsOnly)
		USceneComponent* Root;

	UPROPERTY(BlueprintReadOnly)
		TMap<int, FVector> HexLocations;

	UPROPERTY()
		FMeshInfo MeshInfo;

	UPROPERTY()
		int CurrentUVStartId;

	UPROPERTY()
		int CurrentTexID;

	UPROPERTY()
		int CurrentUVNum;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void CreateMesh(TArray<FJsonStruct>& jsonStructs);

	FMeshInfo PrepareHexArray(const FJsonStruct& jsonStruct, int& TriangleID);

	TArray<FVector2D> GetUVByTexId(int TexId, int CellsNum);

	UFUNCTION(BlueprintCallable)
		FJsonStruct CheckLocation(TArray<FJsonStruct> JsonStructs, FVector Location);

	UFUNCTION(BlueprintCallable)
		void Highlight(int TexId, int CellNum, int StartId);

};
