// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Dom/JsonObject.h"
#include "ProceduralMeshComponent.h"
#include "Structs.generated.h"

USTRUCT()
struct FCellStruct
{
	GENERATED_BODY()

		UPROPERTY()
		float x = 0.f;

	UPROPERTY()
		float y = 0.f;

	UPROPERTY()
		float z = 0.f;

	void SetCell(TArray<TSharedPtr<FJsonValue>> cell)
	{
		x = cell[0]->AsNumber() * 5000.f;
		y = cell[1]->AsNumber() * 5000.f;
		z = cell[2]->AsNumber() * 5000.f;
	}

	FCellStruct()
	{
	}
};

USTRUCT(BlueprintType, Blueprintable)
struct FJsonStruct
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
		int TexId = 0;

	UPROPERTY(BlueprintReadOnly)
		int i = 0;

	UPROPERTY(BlueprintReadOnly)
		float x = 0.f;

	UPROPERTY(BlueprintReadOnly)
		float y = 0.f;

	UPROPERTY(BlueprintReadOnly)
		float z = 0.f;

	UPROPERTY(BlueprintReadOnly)
		float av = 0.f;

	UPROPERTY(BlueprintReadOnly)
		float ah = 0.f;

	UPROPERTY()
		TArray<int> Others;

	UPROPERTY()
		TArray<FCellStruct> Cells;

	void SetCells(TArray<TSharedPtr<FJsonValue>> jsonCells)
	{
		for (auto jsonCell : jsonCells)
		{
			TArray<TSharedPtr<FJsonValue>> cellValue = jsonCell->AsArray();

			FCellStruct cellStruct;

			cellStruct.SetCell(cellValue);

			Cells.Add(cellStruct);
		}
	}

	FJsonStruct()
	{
	}
};

USTRUCT()
struct FMeshInfo
{
	GENERATED_BODY()

	UPROPERTY()
		TArray<FVector> Vertices;

	UPROPERTY()
		TArray<int> Triangles;

	UPROPERTY()
		TArray<FVector2D> UV0;

	UPROPERTY()
		TArray<FColor> Colors;

	UPROPERTY()
		TArray<FVector> Normales;

	UPROPERTY()
		TArray<FProcMeshTangent> Tangents;

	void AddTriangle(int V1, int V2, int V3)
	{
		Triangles.Add(V1);
		Triangles.Add(V2);
		Triangles.Add(V3);
	}

	FMeshInfo()
	{
	}
};

/**
 * 
 */
UCLASS()
class SKAZA_API UStructs : public UObject
{
	GENERATED_BODY()
	
};
