// Fill out your copyright notice in the Description page of Project Settings.


#include "skazaGameModeBase.h"
#include "Public/Actors/WorldActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/StaticMesh.h"
#include "StaticMeshResources.h"
#include "Rendering/PositionVertexBuffer.h"
#include "Materials/Material.h"
#include "Materials/MaterialInstanceDynamic.h"





TArray<FJsonStruct> AskazaGameModeBase::GetStructs()
{
	return Array;
}

void AskazaGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	const FString Path = FPaths::ProjectContentDir() + "pl.json";

	FString JsonString;

	FFileHelper::LoadFileToString(JsonString, *Path);

	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
	TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create(JsonString);

	if (FJsonSerializer::Deserialize(JsonReader, JsonObject), JsonObject.IsValid())
	{
		WorldActor = GetWorld()->SpawnActor<AWorldActor>(AWorldActor::StaticClass(), FVector(0.f, 0.f, 0.f), FRotator());

		TArray<TSharedPtr<FJsonValue>> ValueArray = JsonObject->GetArrayField("array");

		for (auto val : ValueArray)
		{
			TSharedPtr<FJsonObject> Object = val->AsObject();
			TArray<TSharedPtr<FJsonValue>> jsonCells = Object->GetArrayField("cell");

			FJsonStruct jsonStruct;
			jsonStruct.x = Object->GetNumberField("x") * 5000.f;
			jsonStruct.y = Object->GetNumberField("y") * 5000.f;
			jsonStruct.z = Object->GetNumberField("z") * 5000.f;
			jsonStruct.TexId = Object->GetIntegerField("tex_id");
			jsonStruct.ah = Object->GetNumberField("ah");
			jsonStruct.av = Object->GetNumberField("av");
			jsonStruct.i = Object->GetIntegerField("i");
			jsonStruct.SetCells(jsonCells);

			Array.Add(jsonStruct);
		}

		int counter = 0;

		FActorSpawnParameters params;
		params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		Sphere = GetWorld()->SpawnActor<AChunkActorBase>(AChunkActorBase::StaticClass(), FVector(0.f, 0.f, 0.f), FRotator(), params);
		Sphere->CreateMesh(Array);

		UMaterialInstanceDynamic* InstanceMaterial = UMaterialInstanceDynamic::Create(Material, this);

		Sphere->Mesh1->SetMaterial(0, InstanceMaterial);
	}

	
}

