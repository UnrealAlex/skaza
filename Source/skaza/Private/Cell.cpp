// Fill out your copyright notice in the Description page of Project Settings.


#include "Cell.h"
#include "DrawDebugHelpers.h"

// Sets default values
ACell::ACell()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/*Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;*/

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	RootComponent = SphereCollision;
	//SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->SetSphereRadius(40.f);
	SphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	//SphereCollision->SetVisibility(true);
	//SphereCollision->bHiddenInGame = false;
	SphereCollision->SetGenerateOverlapEvents(true);
}

// Called when the game starts or when spawned
void ACell::BeginPlay()
{
	Super::BeginPlay();

	//DrawDebugSphere(GetWorld(), GetActorLocation(), 100.f, 5, FColor::Red, false, 30.f, 0, 1.f);
	
}

// Called every frame
void ACell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

