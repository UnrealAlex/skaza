// Fill out your copyright notice in the Description page of Project Settings.


#include "Public/Actors/ChunkActorBase.h"


// Sets default values
AChunkActorBase::AChunkActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	Mesh1 = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("ChunkMesh1"));
	Mesh1->SetupAttachment(RootComponent);
	Mesh1->bUseAsyncCooking = true;

	//RootComponent = Mesh;
}

// Called when the game starts or when spawned
void AChunkActorBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AChunkActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AChunkActorBase::CreateMesh(TArray<FJsonStruct>& jsonStructs)
{
	int HexId = 0;

	

	for (int i = 0; i < jsonStructs.Num(); i++)
	{
		if (i < jsonStructs.Num())
		{
			FMeshInfo meshInfo = PrepareHexArray(jsonStructs[i], HexId);

			MeshInfo.Triangles.Append(meshInfo.Triangles);
			MeshInfo.Vertices.Append(meshInfo.Vertices);
			MeshInfo.UV0.Append(meshInfo.UV0);
		}

		//HexLocations.Add(jsonStructs[i].i, GetActorLocation() + GetTransform().TransformPosition(FVector(jsonStructs[i].x, jsonStructs[i].y, jsonStructs[i].z)));
	}

	//int f = 0;

	for (int j = 0; j < MeshInfo.Vertices.Num(); j++)
	{
		MeshInfo.Colors.Add(FColor(0.f, 0.f, 0.f, 1.f));
		MeshInfo.Tangents.Add(FProcMeshTangent(0.f, 1.f, 0.f));
		MeshInfo.Normales.Add(FVector(0.f, 0.f, 1.f));

		HexLocations.Add(j, MeshInfo.Vertices[j]);
	}

	Mesh1->CreateMeshSection(0, MeshInfo.Vertices, MeshInfo.Triangles, MeshInfo.Normales, MeshInfo.UV0, MeshInfo.Colors, MeshInfo.Tangents, true);
	Mesh1->SetCastShadow(false);
	Mesh1->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Mesh1->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);

	
}

FMeshInfo AChunkActorBase::PrepareHexArray(const FJsonStruct& jsonStruct, int& HexID)
{
	FMeshInfo meshInfo;

	meshInfo.Vertices.Add(FVector(jsonStruct.x, jsonStruct.y, jsonStruct.z));
	meshInfo.Colors.Add(FColor(1.f, 1.f, 1.f, 1.f));

	for (auto&& cell : jsonStruct.Cells)
	{
		meshInfo.Vertices.Add(FVector(cell.x, cell.y, cell.z));
	}

	for (int i = HexID; i < HexID + meshInfo.Vertices.Num() - 2; i++)
	{
		meshInfo.AddTriangle(i + 1, HexID, i + 2);
	}

	meshInfo.AddTriangle(HexID + meshInfo.Vertices.Num() - 1, HexID, HexID + 1);

	meshInfo.UV0 = GetUVByTexId(jsonStruct.TexId, jsonStruct.Cells.Num());

	HexID += meshInfo.Vertices.Num();

	return meshInfo;
}

FJsonStruct AChunkActorBase::CheckLocation(TArray<FJsonStruct> JsonStructs, FVector Location)
{
	FVector loc = Location;

	FJsonStruct* jsonStruct = nullptr;

	jsonStruct = JsonStructs.FindByPredicate([&,loc](FJsonStruct jStruct)
		{
			return   (GetActorLocation() + GetTransform().TransformPosition(FVector(jStruct.x, jStruct.y, jStruct.z)) - loc).Size() < 50.f;
		});


	if (jsonStruct)
	{
		Highlight(jsonStruct->TexId, jsonStruct->Cells.Num(), *HexLocations.FindKey(FVector(jsonStruct->x, jsonStruct->y, jsonStruct->z)));

		return *jsonStruct;
	}
	else
	{
		return FJsonStruct();
	}

}

void AChunkActorBase::Highlight(int TexId, int CellNum, int StartId)
{
	if (CurrentUVStartId != StartId)
	{
		TArray<FVector2D> OldHighLightUV = GetUVByTexId(CurrentTexID, CurrentUVNum);

		for (int i = CurrentUVStartId; i <= CurrentUVStartId + CurrentUVNum; i++)
		{
			MeshInfo.UV0[i] = OldHighLightUV[i - CurrentUVStartId];
		}

		Mesh1->UpdateMeshSection(0, MeshInfo.Vertices, MeshInfo.Normales, MeshInfo.UV0, MeshInfo.Colors, MeshInfo.Tangents);

		CurrentTexID = TexId;
		CurrentUVStartId = StartId;
		CurrentUVNum = CellNum;



		TArray<FVector2D> HighLightUV = GetUVByTexId(8, CellNum);

		for (int i = StartId; i <= StartId + CellNum; i++)
		{
			MeshInfo.UV0[i] = HighLightUV[i - StartId];
		}

		Mesh1->UpdateMeshSection(0, MeshInfo.Vertices, MeshInfo.Normales, MeshInfo.UV0, MeshInfo.Colors, MeshInfo.Tangents);
	}
}

TArray<FVector2D> AChunkActorBase::GetUVByTexId(int TexId, int CellsNum)
{
	TArray<FVector2D> result;

	switch (TexId)
	{
	case 1 : 
		if (CellsNum == 5)
		{
			result.Add(FVector2D(0.14f, 0.16f));
			result.Add(FVector2D(0.14f, 0.33f));
			result.Add(FVector2D(0.28f, 0.25f));
			result.Add(FVector2D(0.28f, 0.08f));
			result.Add(FVector2D(0.14f, 0.f));
			result.Add(FVector2D(0.f, 0.08f));
		}
		else
		{
			result.Add(FVector2D(0.14f, 0.16f));
			result.Add(FVector2D(0.14f, 0.33f));
			result.Add(FVector2D(0.28f, 0.25f));
			result.Add(FVector2D(0.28f, 0.08f));
			result.Add(FVector2D(0.14f, 0.f));
			result.Add(FVector2D(0.f, 0.08f));
			result.Add(FVector2D(0.f, 0.25f));
		}
		break;

	case 2:
		if (CellsNum == 5)
		{
			result.Add(FVector2D(0.44f, 0.16f));
			result.Add(FVector2D(0.44f, 0.33f));
			result.Add(FVector2D(0.58f, 0.25f));
			result.Add(FVector2D(0.58f, 0.08f));
			result.Add(FVector2D(0.44f, 0.f));
			result.Add(FVector2D(0.3f, 0.08f));
		}
		else
		{
			result.Add(FVector2D(0.44f, 0.16f));
			result.Add(FVector2D(0.44f, 0.33f));
			result.Add(FVector2D(0.58f, 0.25f));
			result.Add(FVector2D(0.58f, 0.08f));
			result.Add(FVector2D(0.44f, 0.f));
			result.Add(FVector2D(0.3f, 0.08f));
			result.Add(FVector2D(0.3f, 0.25f));
		}
		break;

	case 3:
		if (CellsNum == 5)
		{
			result.Add(FVector2D(0.74f, 0.16f));
			result.Add(FVector2D(0.74f, 0.33f));
			result.Add(FVector2D(0.88f, 0.25f));
			result.Add(FVector2D(0.88f, 0.08f));
			result.Add(FVector2D(0.74f, 0.f));
			result.Add(FVector2D(0.6f, 0.08f));
		}
		else
		{
			result.Add(FVector2D(0.74f, 0.16f));
			result.Add(FVector2D(0.74f, 0.33f));
			result.Add(FVector2D(0.88f, 0.25f));
			result.Add(FVector2D(0.88f, 0.08f));
			result.Add(FVector2D(0.74f, 0.f));
			result.Add(FVector2D(0.6f, 0.08f));
			result.Add(FVector2D(0.6f, 0.25f));
		}
		break;

	case 4:
		if (CellsNum == 5)
		{
			result.Add(FVector2D(0.30f, 0.43f));
			result.Add(FVector2D(0.30f, 0.59f));
			result.Add(FVector2D(0.43f, 0.51f));
			result.Add(FVector2D(0.43f, 0.34f));
			result.Add(FVector2D(0.3f, 0.26f));
			result.Add(FVector2D(0.15f, 0.34f));
		}
		else
		{
			result.Add(FVector2D(0.30f, 0.43f));
			result.Add(FVector2D(0.30f, 0.59f));
			result.Add(FVector2D(0.43f, 0.51f));
			result.Add(FVector2D(0.43f, 0.34f));
			result.Add(FVector2D(0.3f, 0.26f));
			result.Add(FVector2D(0.15f, 0.34f));
			result.Add(FVector2D(0.15f, 0.51f));
		}
		break;

	case 5:
		if (CellsNum == 5)
		{
			result.Add(FVector2D(0.59f, 0.43f));
			result.Add(FVector2D(0.59f, 0.59f));
			result.Add(FVector2D(0.73f, 0.51f));
			result.Add(FVector2D(0.73f, 0.34f));
			result.Add(FVector2D(0.59f, 0.26f));
			result.Add(FVector2D(0.45f, 0.34f));
		}
		else
		{
			result.Add(FVector2D(0.59f, 0.43f));
			result.Add(FVector2D(0.59f, 0.59f));
			result.Add(FVector2D(0.73f, 0.51f));
			result.Add(FVector2D(0.73f, 0.34f));
			result.Add(FVector2D(0.59f, 0.26f));
			result.Add(FVector2D(0.45f, 0.34f));
			result.Add(FVector2D(0.45f, 0.51f));
		}
		break;

	case 6:
		if (CellsNum == 5)
		{
			result.Add(FVector2D(0.14f, 0.69f));
			result.Add(FVector2D(0.14f, 0.85f));
			result.Add(FVector2D(0.28f, 0.77f));
			result.Add(FVector2D(0.28f, 0.60f));
			result.Add(FVector2D(0.14f, 0.52f));
			result.Add(FVector2D(0.f, 0.60f));
		}
		else
		{
			result.Add(FVector2D(0.14f, 0.69f));
			result.Add(FVector2D(0.14f, 0.85f));
			result.Add(FVector2D(0.28f, 0.77f));
			result.Add(FVector2D(0.28f, 0.60f));
			result.Add(FVector2D(0.14f, 0.52f));
			result.Add(FVector2D(0.f, 0.60f));
			result.Add(FVector2D(0.f, 0.77f));
		}
		break;

	case 7:
		if (CellsNum == 5)
		{
			result.Add(FVector2D(0.44f, 0.69f));
			result.Add(FVector2D(0.44f, 0.85f));
			result.Add(FVector2D(0.58f, 0.77f));
			result.Add(FVector2D(0.58f, 0.60f));
			result.Add(FVector2D(0.44f, 0.52f));
			result.Add(FVector2D(0.3f, 0.60f));
		}
		else
		{
			result.Add(FVector2D(0.44f, 0.69f));
			result.Add(FVector2D(0.44f, 0.85f));
			result.Add(FVector2D(0.58f, 0.77f));
			result.Add(FVector2D(0.58f, 0.60f));
			result.Add(FVector2D(0.44f, 0.52f));
			result.Add(FVector2D(0.3f, 0.60f));
			result.Add(FVector2D(0.3f, 0.77f));
		}
		break;

	case 8: // Highlight
		if (CellsNum == 5)
		{
			result.Add(FVector2D(0.74f, 0.67f));
			result.Add(FVector2D(0.74f, 0.85f));
			result.Add(FVector2D(0.88f, 0.77f));
			result.Add(FVector2D(0.88f, 0.60f));
			result.Add(FVector2D(0.74f, 0.52f));
			result.Add(FVector2D(0.60f, 0.60f));
		}
		else
		{
			result.Add(FVector2D(0.74f, 0.67f));
			result.Add(FVector2D(0.74f, 0.85f));
			result.Add(FVector2D(0.88f, 0.77f));
			result.Add(FVector2D(0.88f, 0.60f));
			result.Add(FVector2D(0.74f, 0.52f));
			result.Add(FVector2D(0.60f, 0.60f));
			result.Add(FVector2D(0.60f, 0.77f));
		}
		break;

	default :
		if (CellsNum == 5)
		{
			result.Add(FVector2D(0.14f, 0.16f));
			result.Add(FVector2D(0.14f, 0.33f));
			result.Add(FVector2D(0.28f, 0.25f));
			result.Add(FVector2D(0.28f, 0.08f));
			result.Add(FVector2D(0.14f, 0.f));
			result.Add(FVector2D(0.f, 0.08f));
		}
		else
		{
			result.Add(FVector2D(0.14f, 0.16f));
			result.Add(FVector2D(0.14f, 0.33f));
			result.Add(FVector2D(0.28f, 0.25f));
			result.Add(FVector2D(0.28f, 0.08f));
			result.Add(FVector2D(0.14f, 0.f));
			result.Add(FVector2D(0.f, 0.08f));
			result.Add(FVector2D(0.f, 0.25f));
		}
		break;
	}

	return result;
}



