// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Dom/JsonObject.h"
#include "Serialization/JsonReader.h"
#include "Misc/Paths.h"
#include "Misc/FileHelper.h"
#include "Serialization/JsonSerializer.h"
#include "Public/Actors/ChunkActorBase.h"
#include "Public/Structs.h"
#include "skazaGameModeBase.generated.h"


UCLASS()
class SKAZA_API AskazaGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly)
		TArray<FJsonStruct> Array;

	UPROPERTY(BlueprintReadOnly)
		AChunkActorBase* Sphere;

	UPROPERTY(BlueprintReadOnly)
		AActor* WorldActor;

	UPROPERTY(EditDefaultsOnly)
		UMaterial* Material;

	UFUNCTION(BlueprintCallable)
		TArray<FJsonStruct> GetStructs();

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
};
